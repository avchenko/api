class Api::AccountsController < ApplicationController
    before_action :authenticate_with_token!, only: [:create]
    respond_to :json
    
  def index
    accounts = params[:account_ids].present? ? Account.find(params[:account_ids]) : Account.all
    respond_with accounts
  end


    
    def show
        respond_with Account.find(params[:id])
    end
    
    def create
        account = current_customer.accounts.build(account_params)
            if account.save
                render json: account, status: 201, location: [:api, account]
            else
                render json: { errors: account.errors }, status: 422
            end
    end
    
    def update
        account = current_customer.accounts.find(params[:id])
        if account.update(account_params)
            render json: account, status: 200, location: [:api, account]
        else
            render json: { errors: account.errors }, status: 422
        end
    end


  private
    def account_params
        params.require(:account).permit(:name, :sort, :number, :balance)
    end

    
    
end
