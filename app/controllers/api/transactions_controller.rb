class Api::TransactionsController < ApplicationController
    before_validation :set_total!
    before_action :authenticate_with_token!, only: [:create, :update]
    
    
    respond_to :json
    
    def current_account
        @current_account ||= Account.find_by(params[:id])
    end
    
    def index
        respond_with Transaction.all
          
    end
    
    def show
        respond_with Transaction.find(params[:id])
    end
    
    def create
    transaction = current_account.transactions.build(transaction_params)
        if transaction.save
            render json: transaction, status: 201, location: [:api, current_user, current_account, transaction]
        else
            render json: { errors: transaction.errors }, status: 422
        end
    end
    
    def update
    transaction = current_account.transactions.find(params[:id])
        if transaction.update(transaction_params)
            render json: transaction, status: 200, location: [:api, transaction]
        else
            render json: { errors: product.transaction }, status: 422
        end
    end

  private

    def transaction_params
      params.require(:transaction).permit(:total, :debit, :date, :description, :balance, :account_ids =>[])
    end
 
    
end
