class Api::CustomersController < ApplicationController
    before_action :authenticate_with_token!, only: [:update, :destroy]
    respond_to :json
    
    def show
        respond_with Customer.find(params[:id])
    end
    
    def create
        customer = Customer.new(customer_params)
        if customer.save
            render json: customer, status: 201, location: [:api, customer]
        else
            render json: { errors: customer.errors }, status: 422
        end
    end
    
    def update
        customer = current_customer

        if customer.update(customer_params)
            render json: customer, status: 200, location: [:api, customer]
        else
            render json: { errors: customer.errors }, status: 422
        end
    end
    
    def destroy
        customer = Customer.find(params[:id])
        customer.destroy
        head 204
    end
    

    private
    def customer_params
      params.require(:customer).permit(:email, :password, :password_confirmation, :address, :cust_name)
    end
    


end
