class Transaction < ActiveRecord::Base
    belongs_to :account
  
    validates :total, presence: true,
                      numericality: { greater_than_or_equal_to: 0 }

    validates :account_id, presence: true
    
    def set_total!
        self.total = accounts.map(&:balance).sum
    end
end
