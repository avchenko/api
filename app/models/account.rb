class Account < ActiveRecord::Base
    validates :name, :customer_id, presence: true
    validates :balance, numericality: { greater_than_or_equal_to: 0 },
                        presence: true
    belongs_to :customer
    has_many :transactions
    
end
