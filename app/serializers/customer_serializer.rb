class CustomerSerializer < ActiveModel::Serializer
    embed :ids
    attributes :id, :email, :created_at, :updated_at, :auth_token, :address, :cust_name
    
    has_many :accounts
end
