class TransactionSerializer < ActiveModel::Serializer
  attributes :id, :total, :description, :date, :balance
  belongs_to :account
end
