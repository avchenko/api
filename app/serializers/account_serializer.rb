class AccountSerializer < ActiveModel::Serializer
  attributes :id, :name, :sort, :number, :balance
  has_one :customer
end
