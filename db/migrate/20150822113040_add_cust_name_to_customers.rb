class AddCustNameToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :cust_name, :string, default: ""
  end
end
