class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.decimal :total, default: 0.0
      t.boolean :debit, default: false
      t.date :date
      t.string :description
      t.decimal :balance, default: 0.0
      t.references :account, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
