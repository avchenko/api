class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :name, default: ""
      t.integer :sort
      t.integer :number
      t.decimal :balance, default: 0.0
      t.integer :customer_id

      t.timestamps null: false
    end
    add_index :accounts, :customer_id
  end
end
